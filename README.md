
# Keyboard commands
    q / ESC     Quit
    c           Clear screen (with black)
    F11         Toggle fullscreen mode
    F12         Switch monitor in fullscreen mode


# Building from source

    $ sudo apt-get install build-essential cmake git
    $ sudo apt-get install libevent-dev libssl-dev libglew-dev xorg-dev libglu1-mesa-dev
    $ git submodule init
    $ git submodule update
    $ make
    $ ./release/pixelnuke

